# PGRF3 Úloha 2 - Environment mapping  
[GitLab repository](https://gitlab.com/goryllaz/pgrf3-uloha2)  
[Last version of build](https://gitlab.com/goryllaz/pgrf3-uloha2/raw/build/PGRF3%20Uloha2%20v1.0.0.jar)  

## Dependencies   
* JOGL 2.3.2  
* Vavr.io 1.0.0    
 
## Control  
__Camera angle:__ mouse  
__Camera forward:__ W  
__Camera backwards:__ S  
__Camera left:__ A  
__Camera right:__ D  
__Camera up:__ Shift  
__Camera down:__ Control  
