#version 150
in vec3 vertColor;
in vec3 vertPosition;

out vec4 outColor;

uniform samplerCube cubeTex;

vec3 swapYZflipZ(vec3 x) {
    mat4 swapMat = mat4(
            1, 0, 0, 0,
            0, 0, -1, 0,
            0, 1, 0, 0,
            0, 0, 1, 1
     );

     return (vec4(x, 1) * swapMat).xyz;
}

void main() {
    vec3 matDifCol = vec3(0.5);
    vec3 ambientLightCol = vec3(0.1);

    vec3 difComponent = texture(cubeTex, swapYZflipZ(vertPosition)).xyz * matDifCol;
    vec3 ambiComponent = ambientLightCol;
	outColor = vec4(ambiComponent + difComponent, 1);
	outColor = texture(cubeTex, swapYZflipZ(vertPosition));
}

	 
