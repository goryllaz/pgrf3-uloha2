#version 150
in vec3 vertColor;
in vec3 vertNormal;
in vec3 vertPosition;
in vec3 eyeVec;
in vec3 lightVec;
in vec3 reflection;
in vec3 refraction;
in vec3 refractionR;
in vec3 refractionB;
in float fresnel;

out vec4 outColor;

uniform vec3 eyePos;

uniform sampler2D diffTex;
uniform sampler2D normTex;
uniform sampler2D heightTex;
uniform samplerCube cubeTex;
uniform sampler2D texRGB;
uniform samplerCube envTex;

uniform vec3 lightPos;
uniform float lightCutoff;
uniform float lightDist;

uniform int texSet;
uniform int isTex;
uniform int shaderSet;
uniform int parallaxSet;
uniform int envMapSet;
uniform int isOcclusion;
uniform int isEnv;

uniform vec3 diff;
uniform vec3 spec;
uniform vec3 ambi;
uniform vec3 lightCol;


void main() {
    if(shaderSet == 0 && isEnv == 0) {
        vec3 matDifCol = diff;
        vec3 matSpecCol = spec;
        vec3 ambientLightCol = ambi;
        vec3 directLightCol = lightCol;

        float scaleL = 0.04;
        float scaleK = -0.02;

        switch (parallaxSet) {
            case 0:
                scaleL = 0.02;
                scaleK = 0.0;
                break;
            case 1:
                scaleL = 0.04;
                scaleK = -0.02;
                break;
            case 2:
                scaleL = 0.09;
                scaleK = 0.0;
                break;
        }

        vec2 texCoord = vertColor.xy * vec2(1,-1) + vec2(0,1);

        float height = texture(heightTex, texCoord).r;
        float v = height * scaleL + scaleK;
        vec3 eye = normalize(eyeVec);
        vec2 offset = eye.xy * v;
        texCoord = texCoord + offset;

        if( isOcclusion == 1 && isTex == 1 &&(texCoord.x > 1 || texCoord.y > 1 || texCoord.x < 0 || texCoord.y < 0))
            discard;

        vec3 inNormal = normalize(vertNormal);


        vec3 cubeTexCoord = normalize(reflection);
        if (isTex == 1)
            cubeTexCoord += eye.xyz * v;

        if (isTex == 1) {
            inNormal = texture(normTex, texCoord).xyz * 2 - 1;
            matDifCol = texture(diffTex, texCoord).xyz * matDifCol;
        }

        vec3 lVec = normalize(lightVec);

        vec3 ambiComponent = ambientLightCol * matDifCol;

        vec3 tmpLVec = lVec;
        if (isTex == 0)
            lVec = normalize(lightPos - vertPosition);
        float difCoef = pow(max(0, lVec.z), 0.7) * max(0, dot(inNormal, lVec));
        vec3 difComponent = directLightCol * matDifCol * difCoef;

        if (isTex == 0)
            lVec = -normalize(vertPosition - lightPos);
        vec3 reflected = reflect(-lVec, inNormal);

        lVec = tmpLVec;

        vec3 tmpEyeVec = eyeVec;
        if (isTex == 0)
            tmpEyeVec = eyePos - vertPosition;
        float specCoef = pow(max(0, lVec.z), 0.7) * pow(max(0,
            dot(normalize(tmpEyeVec), reflected)
        ), 70);
        vec3 specComponent = directLightCol * matSpecCol * specCoef;

        outColor.rgb = ambiComponent + difComponent + specComponent;
    }
	if(shaderSet == 1 && isEnv == 0 || shaderSet == 2 && isEnv == 0) {
	    outColor =  vec4(vertColor, 1);
	}

	if(isEnv == 1) {
         vec3 reflectTex = texture(cubeTex, normalize(reflection)).rgb;
         vec3 refractTex = texture(cubeTex, normalize(refraction)).rgb;
         if(envMapSet == 0)
            outColor = vec4(reflectTex, 1);
         if(envMapSet == 1)
            outColor = vec4(refractTex, 1);
         if(envMapSet == 2)
            outColor = vec4(mix(reflectTex, refractTex, fresnel), 1);
         if(envMapSet == 3) {
            refractTex.r = texture(cubeTex, normalize(refractionR)).r;
            refractTex.g = texture(cubeTex, normalize(refraction)).g;
            refractTex.b = texture(cubeTex, normalize(refractionB)).b;
            outColor = vec4(mix(reflectTex, refractTex, fresnel), 1);
         }
         if(envMapSet == 4) {
            vec3 lVec = normalize(lightVec);
            float difCoef = pow(max(0, lVec.z), 0.7) * max(0, dot(normalize(vertNormal), lVec));
            vec3 difComponent = lightCol * diff * difCoef;
            outColor = vec4(mix(reflectTex, difComponent, 0.4) * diff, 1);
         }
    }
}
