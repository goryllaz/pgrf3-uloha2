#version 150
in vec2 inParamPos;

out vec3 vertColor;
out vec3 vertNormal;
out vec3 vertPosition;
out vec3 eyeVec;
out vec3 lightVec;
out vec3 reflection;
out vec3 refraction;
out vec3 refractionR;
out vec3 refractionB;
out float fresnel;


uniform mat4 mat;
uniform mat4 tMat;
uniform mat4 proj;
uniform vec3 eyePos;
uniform float time;
uniform float refractIndex;

uniform sampler2D diffTex;
uniform sampler2D normTex;

uniform vec3 lightDir;
uniform vec3 lightPos;
uniform float lightCutoff;
uniform float lightDist;

uniform int shapeSet;
uniform int texSet;
uniform int isTex;
uniform int animSet;
uniform int shaderSet;
uniform int parallaxSet;
uniform int envMapSet;

const float PI = 3.1415926536;

float blob(float z) {
    return sin(time * 2 + z * 5) * 0.1;
}

vec3 cone(vec2 paramPos) {
    float a = 2 * PI * paramPos.x;
    float t = paramPos.y;

    if (animSet == 2)
        a += time;
    float x = -t * cos(a);
    float y = -t * sin(a);
    float z = -t;
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 snake(vec2 paramPos) {
    float a = paramPos.x ;
    float t = -2 * PI * paramPos.y;

    float x = ((1-a)*(3+cos(t))*cos(2*PI*a)) * 0.5;
    float y = ((1-a)*(3+cos(t))*sin(2*PI*a)) * 0.5;
    float z = (6*a+(1-a)*sin(t)) * 0.5 - 1;
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 trumpet(vec2 paramPos) {
    float a =  -2 * PI * paramPos.x;
    float t = paramPos.y * 3 ;

    if (animSet == 2)
        a += time;
    float x = 1/(pow(t+1, 1.5))*cos(a);
    float y = 1/(pow(t+1, 1.5))*sin(a);
    float z = t - 1.5;
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 plane(vec2 paramPos) {
    float a = paramPos.x * 4 - 2;
    float t = paramPos.y * 4 - 2;

    float x = a;
    float y = 0;
    float z = t;
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 sphere(vec2 paramPos) {
    float a = 2 * PI * paramPos.x;
    float t = PI * paramPos.y ;

    if (animSet == 2)
        a += time;
    float x = cos(a) * sin(t);
    float y = sin(a) * sin(t);
    float z = cos(t);
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 tunnel(vec2 paramPos) {
    float a = 2 * PI * paramPos.x;
    float t = PI * paramPos.y ;
    float rho = exp(- pow(t-PI / 2,2));

    if (animSet == 2)
        a += time;
    float x = cos(a) * sin(t) * rho;
    float y = sin(a) * sin(t) * rho;
    float z = cos(t) * rho;
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 spaceStation(vec2 paramPos) {
    float a = 2 * PI * paramPos.x;
    float t = PI * paramPos.y ;
    float rho = 1 + 0.5 * sin(4 * t);

    if (animSet == 2)
        a += time;
    float x = cos(a) * sin(t) * rho;
    float y = sin(a) * sin(t) * rho;
    float z = cos(t) * rho;
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 earring(vec2 paramPos) {
    float a = 2 * PI * paramPos.x;
    float t = PI * paramPos.y ;
    float rho = abs(t - PI / 2);

    if (animSet == 2)
        a += time;
    float x = cos(a) * sin(t) * rho;
    float y = sin(a) * sin(t) * rho;
    float z = cos(t) * rho;
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 cylinder(vec2 paramPos) {
    float a = 2 * PI * paramPos.x;
    float t = 1 - paramPos.y;

    if (animSet == 2)
        a += time;
    float x = cos(a);
    float y = sin(a);
    float z = t * 3 - 1.5;
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 juicer(vec2 paramPos) {
    float a = 2 * PI * paramPos.x;
    float t = 2 * PI * paramPos.y;
    float r = t;

    if (animSet == 2)
        a += time;
    float x = r * cos(a) * 0.5;
    float y = r * sin(a) * 0.5;
    float z = cos(t) * 0.5;
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 sombrero(vec2 paramPos) {
    float a = 2 * PI * paramPos.x;
    float t = 2 * PI * paramPos.y;
    float r = t;

    if (animSet == 2)
        a += time;
    float x = r * cos(a) * 0.5;
    float y = r * sin(a) * 0.5;
    float z = 2 * sin(t) * 0.5;
    if (animSet == 1)
        x += blob(z);
    return vec3(
        x ,
        y ,
        z
    );
}

vec3 surface(vec2 paramPos) {
    switch(shapeSet) {
        case 0:
            return cone(paramPos);
        case 1:
            return snake(paramPos);
        case 2:
            return trumpet(paramPos);
        case 3:
            return plane(paramPos);
        case 4:
            return sphere(paramPos);
        case 5:
            return tunnel(paramPos);
        case 6:
            return spaceStation(paramPos);
        case 7:
            return earring(paramPos);
        case 8:
            return cylinder(paramPos);
        case 9:
            return juicer(paramPos);
        case 10:
            return sombrero(paramPos);
    }
    return sphere(paramPos);
}

vec3 normal(vec2 paramPos) {
    float d = 1e-5;
    vec2 dx = vec2(d, 0);
    vec2 dy = vec2(0, d);
    vec3 tx = (surface(paramPos + dx) - surface(paramPos - dx)) / (2 * d);
    vec3 ty = (surface(paramPos + dy) - surface(paramPos - dy)) / (2 * d);
    return cross(ty, tx);
}

mat3 tangentMat(vec2 paramPos) {
    float d = 1e-5;
    vec2 dx = vec2(d, 0);
    vec2 dy = vec2(0, d);
    vec3 tx = (surface(paramPos + dx) - surface(paramPos - dx)) / (2 * d);
    vec3 ty = (surface(paramPos + dy) - surface(paramPos - dy)) / (2 * d);
    vec3 x = normalize(tx);
    vec3 y = normalize(-ty);
    vec3 z = cross(x, y);
    x = cross(y, z);
    return mat3(x,y,z);
}

vec3 swapYZflipZ(vec3 x) {
    mat4 swapMat = mat4(
            1, 0, 0, 0,
            0, 0, -1, 0,
            0, 1, 0, 0,
            0, 0, 1, 1
     );

     return (vec4(x, 1) * swapMat).xyz;
}

vec3 swapYZflipZ2(vec3 x) {
    mat4 swapMat = mat4(
            1, 0, 0, 0,
            0, 0, 1, 0,
            0, 1, 0, 0,
            0, 0, 1, 1
     );

     return (vec4(x, 1) * swapMat).xyz;
}

float fastFresnel(vec3 I, vec3 N) {
    float exp = dot(I, N);
    return 10.0 * (exp * exp * exp * exp);
}

void main() {
    vertPosition = (tMat * vec4( surface(inParamPos), 1.0)).xyz;
	gl_Position = mat * vec4(vertPosition, 1.0);

    vertNormal = normal(inParamPos);

    mat3 tanMat = tangentMat(inParamPos);
    eyeVec = (eyePos - vertPosition) * tanMat;
    lightVec = (lightPos - vertPosition) * tanMat;

    reflection = reflect(normalize(swapYZflipZ(vertPosition) - swapYZflipZ(eyePos)), normalize(swapYZflipZ(vertNormal)));
    fresnel = clamp(fastFresnel(normalize(swapYZflipZ(vertPosition) - swapYZflipZ(eyePos)), normalize(swapYZflipZ(vertNormal))), 0, 1);
    refraction = refract(normalize(swapYZflipZ(vertPosition) - swapYZflipZ(eyePos)), normalize(swapYZflipZ(vertNormal)), refractIndex);

    float offset = 0.02;
    refractionR = refract(normalize(swapYZflipZ(vertPosition) - swapYZflipZ(eyePos)), normalize(swapYZflipZ(vertNormal)), refractIndex + offset);
    refractionB = refract(normalize(swapYZflipZ(vertPosition) - swapYZflipZ(eyePos)), normalize(swapYZflipZ(vertNormal)), refractIndex - offset);


	vertColor = vec3(inParamPos,0);

	if (shaderSet == 2) {
        vertColor = normalize(vertNormal) / 2 + 0.5;
    }
}
