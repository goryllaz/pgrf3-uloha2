#version 150
in vec3 inPosition;
in vec3 inNormal;

out vec3 vertColor;
out vec3 vertPosition;

uniform mat4 mat;

void main() {
	vertPosition = 1000*inPosition.xyz-500;
	gl_Position = mat * vec4(vertPosition, 1.0);
	vertColor = inPosition;
} 
