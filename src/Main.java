import cz.goryllaz.graphic.system.AppSystem;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        new AppSystem().start();
    }
}
