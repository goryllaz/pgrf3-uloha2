package cz.goryllaz.graphic.system;

import cz.goryllaz.graphic.renderset.*;
import cz.goryllaz.graphic.renderset.Shape;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class WindowSystem {
    private final JFrame frame;

    public WindowSystem() {
        this.frame = new JFrame();
    }

    private void SetWindow(Command close) {
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                close.execute();
            }
        });
    }

    public void open(Component canvas, ControllerSystem controller, RendererSettings settings, Command close) {
        SetWindow(close);
        JMenuBar menuBar  = new JMenuBar();
        JMenu menuShape = new JMenu("Shape");

        JMenuItem menuCone= new JMenuItem("Cone");
        menuCone.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.CONE);
            }
        });
        JMenuItem menuSnake= new JMenuItem("Snake");
        menuSnake.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.SNAKE);
            }
        });
        JMenuItem menuTrumpet= new JMenuItem("Trumpet");
        menuTrumpet.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.TRUMPET);
            }
        });
        JMenuItem menuPlane= new JMenuItem("Plane");
        menuPlane.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.PLANE);
            }
        });
        JMenuItem menuSphere = new JMenuItem("Sphere");
        menuSphere.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.SPHERE);
            }
        });
        JMenuItem menuTunnel = new JMenuItem("Tunnel");
        menuTunnel.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.TUNNEL);
            }
        });
        JMenuItem menuSpaceStation = new JMenuItem("Space station");
        menuSpaceStation.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.SPACE_STATION);
            }
        });
        JMenuItem menuEarring = new JMenuItem("Earring");
        menuEarring.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.EARRING);
            }
        });
        JMenuItem menuCylinder= new JMenuItem("Cylinder");
        menuCylinder.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.CYLINDER);
            }
        });
        JMenuItem menuJuicer= new JMenuItem("Juicer");
        menuJuicer.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.JUICER);
            }
        });
        JMenuItem menuSombrero= new JMenuItem("Sombrero");
        menuSombrero.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShape(Shape.SOMBRERO);
            }
        });

        menuShape.add(menuCone);
        menuShape.add(menuSnake);
        menuShape.add(menuTrumpet);
        menuShape.add(menuPlane);
        menuShape.add(menuSphere);
        menuShape.add(menuTunnel);
        menuShape.add(menuSpaceStation);
        menuShape.add(menuEarring);
        menuShape.add(menuCylinder);
        menuShape.add(menuJuicer);
        menuShape.add(menuSombrero);

        JMenu menuTexture = new JMenu("Texture");

        JMenuItem menuTexOff = new JMenuItem("Off");
        menuTexOff.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setTexture(false);
                settings.setRedraw(true);
            }
        });
        JMenuItem menuBricks = new JMenuItem("Bricks");
        menuBricks.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setTexture(true);
                settings.setTexture(Texture.BRICKS);
                settings.setRedraw(true);
            }
        });
        JMenuItem menuRock = new JMenuItem("Rock");
        menuRock.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setTexture(true);
                settings.setTexture(Texture.ROCK);
                settings.setRedraw(true);
            }
        });
        JMenuItem menuWall = new JMenuItem("Wall");
        menuWall.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setTexture(true);
                settings.setTexture(Texture.WALL);
                settings.setRedraw(true);
            }
        });

        menuTexture.add(menuTexOff);
        menuTexture.add(menuBricks);
        menuTexture.add(menuRock);
        menuTexture.add(menuWall);

        JMenu menuSkybox = new JMenu("Skybox");

        JMenuItem menuSnow = new JMenuItem("Snow");
        menuSnow.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setSkybox(Skybox.SNOW);
                settings.setRedraw(true);
            }
        });
        JMenuItem menuChurch = new JMenuItem("Church");
        menuChurch.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setSkybox(Skybox.CHURCH);
                settings.setRedraw(true);
            }
        });
        JMenuItem menuForest = new JMenuItem("Forest");
        menuForest.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setSkybox(Skybox.FOREST);
                settings.setRedraw(true);
            }
        });
        JMenuItem menuLake = new JMenuItem("Lake");
        menuLake.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setSkybox(Skybox.LAKE);
                settings.setRedraw(true);
            }
        });

        menuSkybox.add(menuSnow);
        menuSkybox.add(menuChurch);
        menuSkybox.add(menuForest);
        menuSkybox.add(menuLake);

        JMenu menuLight = new JMenu("Light");

        JMenuItem menuStatic = new JMenuItem("Static");
        menuStatic.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setLight(Light.STATIC);
            }
        });
        JMenuItem menuAnimLight = new JMenuItem("Animation");
        menuAnimLight.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setLight(Light.ANIMATION);
            }
        });
        JMenuItem menuCamera = new JMenuItem("Camera");
        menuCamera.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setLight(Light.CAMERA);
            }
        });

        menuLight.add(menuStatic);
        menuLight.add(menuAnimLight);
        menuLight.add(menuCamera);

        JMenu menuAnimation = new JMenu("Animation");

        JMenuItem menuAnimOff = new JMenuItem("Off");
        menuAnimOff.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setAnimation(Animation.OFF);
            }
        });
        JMenuItem menuBlob = new JMenuItem("Blob");
        menuBlob.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setAnimation(Animation.BLOB);
            }
        });
        JMenuItem menuRotation = new JMenuItem("Rotation");
        menuRotation.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setAnimation(Animation.ROTATION);
            }
        });

        menuAnimation.add(menuAnimOff);
        menuAnimation.add(menuBlob);
        menuAnimation.add(menuRotation);

        JMenu menuEnvMap = new JMenu("Env. Map");

        JMenuItem menuReflection = new JMenuItem("Reflection");
        menuReflection.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setEnvMap(EnvMap.REFLECTION);
            }
        });

        JMenuItem menuRefraction = new JMenuItem("Refraction");
        menuRefraction.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setEnvMap(EnvMap.REFRACTION);
            }
        });

        JMenuItem menuGlass = new JMenuItem("Glass");
        menuGlass.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setEnvMap(EnvMap.GLASS);
            }
        });

        JMenuItem menuDispersion = new JMenuItem("Dispersion");
        menuDispersion.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setEnvMap(EnvMap.DISPERSION);
            }
        });

        JMenuItem menuGold = new JMenuItem("Gold");
        menuGold.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setEnvMap(EnvMap.GOLD);
            }
        });

        menuEnvMap.add(menuReflection);
        menuEnvMap.add(menuRefraction);
        menuEnvMap.add(menuGlass);
        menuEnvMap.add(menuDispersion);
        menuEnvMap.add(menuGold);

        JMenu menuShader = new JMenu("Shader");

        JMenuItem menuPerPixel = new JMenuItem("Per-pixel");
        menuPerPixel.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShader(Shader.PER_PIXEL);
                settings.setRedraw(true);
            }
        });
        JMenuItem menuPosition= new JMenuItem("Position");
        menuPosition.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShader(Shader.POSITION);
                settings.setRedraw(true);
            }
        });
        JMenuItem menuNormal = new JMenuItem("Normal");
        menuNormal.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setShader(Shader.NORMAL);
                settings.setRedraw(true);
            }
        });

        menuShader.add(menuPerPixel);
        menuShader.add(menuPosition);
        menuShader.add(menuNormal);

        JMenu menuGenObj = new JMenu("Gen. Objects");

        JMenuItem menuNone = new JMenuItem("None");
        menuNone.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.generateObjects(0);
                settings.setRedraw(true);
            }
        });
        JMenuItem menuTwo = new JMenuItem("Two");
        menuTwo.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.generateObjects(2);
                settings.setRedraw(true);
            }
        });

        JMenuItem menuFive = new JMenuItem("Five");
        menuFive.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.generateObjects(5);
                settings.setRedraw(true);
            }
        });

        JMenuItem menuTen = new JMenuItem("Ten");
        menuTen.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.generateObjects(10);
                settings.setRedraw(true);
            }
        });

        menuGenObj.add(menuNone);
        menuGenObj.add(menuTwo);
        menuGenObj.add(menuFive);
        menuGenObj.add(menuTen);

        JMenu menuParallax = new JMenu("Parallax");

        JMenuItem menuLevel1 = new JMenuItem("[0.02, 0.0]");
        menuLevel1.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setParallax(Parallax.LEVEL1);
            }
        });
        JMenuItem menuLevel2 = new JMenuItem("[0.04, -0.02]");
        menuLevel2.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setParallax(Parallax.LEVEL2);
            }
        });
        JMenuItem menuLevel3 = new JMenuItem("[0.09, 0.0]");
        menuLevel3.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setParallax(Parallax.LEVEL3);
            }
        });
        JMenuItem menuOcclusionOn = new JMenuItem("Occlusion on");
        menuOcclusionOn.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setOcclusion(true);
            }
        });
        JMenuItem menuOcclusionOff = new JMenuItem("Occlusion off");
        menuOcclusionOff.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setOcclusion(false);
            }
        });

        menuParallax.add(menuLevel1);
        menuParallax.add(menuLevel2);
        menuParallax.add(menuLevel3);
        menuParallax.add(menuOcclusionOn);
        menuParallax.add(menuOcclusionOff);

        JMenu menuRefract = new JMenu("Refract");

        JMenuItem menuLow = new JMenuItem("Low");
        menuLow.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setRefract(Refract.LOW);
            }
        });
        JMenuItem menuMedium = new JMenuItem("Medium");
        menuMedium.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setRefract(Refract.MEDIUM);
            }
        });
        JMenuItem menuHigh = new JMenuItem("High");
        menuHigh.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settings.setRefract(Refract.HIGH);
            }
        });

        menuRefract.add(menuLow);
        menuRefract.add(menuMedium);
        menuRefract.add(menuHigh);

        menuBar.add(menuShape);
        menuBar.add(menuTexture);
        menuBar.add(menuSkybox);
        menuBar.add(menuLight);
        menuBar.add(menuAnimation);
        menuBar.add(menuEnvMap);
        menuBar.add(menuShader);
        menuBar.add(menuGenObj);
        menuBar.add(menuParallax);
        menuBar.add(menuRefract);

        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(AppSystem.WIDTH, AppSystem.HEIGHT));

        frame.setJMenuBar(menuBar);
        frame.add(canvas);

        canvas.addKeyListener(controller);
        canvas.addMouseListener(controller);
        canvas.addMouseMotionListener(controller);

        frame.pack();
        frame.setVisible(true);
    }
}