package cz.goryllaz.graphic.renderset;

public enum Skybox {
    SNOW,
    CHURCH,
    FOREST,
    LAKE
}
