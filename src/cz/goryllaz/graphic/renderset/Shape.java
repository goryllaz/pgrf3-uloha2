package cz.goryllaz.graphic.renderset;

public enum Shape {
    CONE,
    SNAKE,
    TRUMPET,
    PLANE,
    SPHERE,
    TUNNEL,
    SPACE_STATION,
    EARRING,
    CYLINDER,
    JUICER,
    SOMBRERO
}
