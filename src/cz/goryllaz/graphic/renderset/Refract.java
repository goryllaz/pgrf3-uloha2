package cz.goryllaz.graphic.renderset;

public enum Refract {
    LOW(0.01f),
    MEDIUM(0.2f),
    HIGH(0.6f);

    private final float index;

    Refract(float index) {
        this.index = index;
    }

    public float getIndex() {
        return index;
    }
}
