package cz.goryllaz.graphic.renderset;

public enum Light {
    STATIC,
    ANIMATION,
    CAMERA
}
