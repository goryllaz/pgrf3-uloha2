package cz.goryllaz.graphic.renderset;

import transforms.Mat4;
import transforms.Mat4Transl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RendererSettings {
    private Shape shape = Shape.SPHERE;
    private Texture texture = Texture.BRICKS;
    private Skybox skybox = Skybox.LAKE;
    private Light light = Light.STATIC;
    private Animation animation = Animation.OFF;
    private EnvMap envMap = EnvMap.GLASS;
    private Shader shader = Shader.PER_PIXEL;
    private Parallax parallax = Parallax.LEVEL2;
    private Refract refract = Refract.MEDIUM;

    private float lightDist = 20;
    private boolean isTexture = false;
    private boolean isOcclusion = false;
    private boolean redraw = true;

    private List<Mat4> objects = new ArrayList<>();

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public Skybox getSkybox() {
        return skybox;
    }

    public void setSkybox(Skybox skybox) {
        this.skybox = skybox;
    }

    public Light getLight() {
        return light;
    }

    public void setLight(Light light) {
        this.light = light;
    }

    public float getLightCutoff() {
        switch (light) {
            case STATIC:
                return 0.8f;
            case ANIMATION:
                return 0.8f;
            case CAMERA:
                return 0.95f;
        }
        return 0.9f;
    }

    public float getLightDist() {
        return lightDist;
    }

    public void setLightDist(float lightDist) {
        this.lightDist = lightDist;
    }

    public Animation getAnimation() {
        return animation;
    }

    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public EnvMap getEnvMap() {
        return envMap;
    }

    public void setEnvMap(EnvMap envMap) {
        this.envMap = envMap;
    }

    public Shader getShader() {
        return shader;
    }

    public void setShader(Shader shader) {
        this.shader = shader;
    }

    public Parallax getParallax() {
        return parallax;
    }

    public void setParallax(Parallax parallax) {
        this.parallax = parallax;
    }

    public Refract getRefract() {
        return refract;
    }

    public void setRefract(Refract refract) {
        this.refract = refract;
    }

    public boolean isTexture() {
        return isTexture;
    }

    public void setTexture(boolean texture) {
        isTexture = texture;
    }

    public boolean isOcclusion() {
        return isOcclusion;
    }

    public void setOcclusion(boolean occlusion) {
        isOcclusion = occlusion;
    }

    public boolean isRedraw() {
        return redraw;
    }

    public void setRedraw(boolean redraw) {
        this.redraw = redraw;
    }

    public List<Mat4> getObjects() {
        return objects;
    }

    public void generateObjects(int count) {
        objects = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            int min = 5;
            int max = 10;
            Random random = new Random();
            int x = (min + random.nextInt(max - min + 1)) * (random.nextBoolean() ? 1 : -1);
            int y = (min + random.nextInt(max - min + 1)) * (random.nextBoolean() ? 1 : -1);
            int z = (min + random.nextInt(max - min + 1)) * (random.nextBoolean() ? 1 : -1);
            objects.add(new Mat4Transl(x, y, z));
        }
    }
}
