package cz.goryllaz.graphic.renderset;

public enum Texture {
    BRICKS,
    ROCK,
    WALL
}
