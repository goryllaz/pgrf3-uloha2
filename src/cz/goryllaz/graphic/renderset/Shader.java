package cz.goryllaz.graphic.renderset;

public enum Shader {
    PER_PIXEL,
    POSITION,
    NORMAL
}
