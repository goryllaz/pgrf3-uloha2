package cz.goryllaz.graphic.renderset;

public enum EnvMap {
    REFLECTION,
    REFRACTION,
    GLASS,
    DISPERSION,
    GOLD
}
