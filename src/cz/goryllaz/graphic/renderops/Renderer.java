package cz.goryllaz.graphic.renderops;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import cz.goryllaz.graphic.renderset.*;
import cz.goryllaz.graphic.system.ControllerSystem;
import cz.goryllaz.graphic.renderutils.MeshGenerator;
import oglutils.*;
import transforms.*;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class Renderer implements GLEventListener {
    private final ControllerSystem controller;
    private final RendererSettings settings;

    private int width, height, polygonMode = GL2GL3.GL_FILL;
    private float time;

    private int skyWidth = 256, skyHeigh = 256;
    private int skyEnum;

    private OGLBuffers grid;
    private OGLBuffers grid2;
    private OGLBuffers skyBox;

    private List<OGLTexture> diffTex, normTex, heightTex;
    private List<OGLTextureCube> cubeTex;
    private OGLTexture2D textureColor;
    private OGLRenderTarget renderTarget;
    private List<OGLTexture2D> envTex;

    private OGLTexImageByte[][] skyTexs = new OGLTexImageByte[Skybox.values().length][6];
    private OGLTexImageByte[][] envTexs = new OGLTexImageByte[Skybox.values().length][6];

    private int shaderGrid, locMatGrid, locTMatGrid, locProjGrid, locTimeGrid, locEyeGrid, locRefractIndex,
            locLightGrid, locLightDirGrid, locLightCutoffGrid, locLightDistGrid,
            locShapeSetGrid, locTexSetGrid, locLightSetGrid, locAnimSetGrid, locShaderSetGrid,
            locParallaxSet, locEnvMapSet,
            locIsTexGrid, locIsOcclusion, locIsEnvGrid,
            locDiff, locSpec, locAmbi, locLightCol;

    private int shaderSky, locMatSky;

    private Mat4 proj;
    private Mat4 envProj;

    public Renderer(ControllerSystem controller, RendererSettings settings) {
        this.controller = controller;
        this.settings = settings;
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2GL3 gl = drawable.getGL().getGL2GL3();

        System.out.println("Init GL is " + gl.getClass().getName());
        System.out.println("OpenGL version " + gl.glGetString(GL2GL3.GL_VERSION));
        System.out.println("OpenGL vendor " + gl.glGetString(GL2GL3.GL_VENDOR));
        System.out
                .println("OpenGL renderer " + gl.glGetString(GL2GL3.GL_RENDERER));
        System.out.println("OpenGL extension "
                + gl.glGetString(GL2GL3.GL_EXTENSIONS));

        if (settings.getShape() == Shape.SPHERE) {
            setShader(gl);
        }

        controller.setCam(
                controller.getCam().withPosition(new Vec3D(-7, -7, 2))
                        .addAzimuth(Math.PI / 4).addZenith(-10 * Math.PI / 180)
        );

        gl.glEnable(GL2GL3.GL_DEPTH_TEST);

        renderTarget = new OGLRenderTarget(gl, skyWidth, skyHeigh);
        
        skyEnum = settings.getSkybox().ordinal();
    }

    void setShader(GL2GL3 gl) {

        shaderSky = ShaderUtils.loadProgram(gl, "/skybox");
        createSkybox(gl);

        locMatSky = gl.glGetUniformLocation(shaderSky, "mat");

        shaderGrid = ShaderUtils.loadProgram(gl, "/program");

        grid = MeshGenerator.generateGrid(gl, 50, 50, "inParamPos");

        grid2 = MeshGenerator.generateGrid(gl, 30, 30, "inParamPos");

        createTexture(gl);

        locMatGrid = gl.glGetUniformLocation(shaderGrid, "mat");
        locTMatGrid = gl.glGetUniformLocation(shaderGrid, "tMat");
        locProjGrid = gl.glGetUniformLocation(shaderGrid, "proj");
        locLightGrid = gl.glGetUniformLocation(shaderGrid, "lightPos");
        locEyeGrid = gl.glGetUniformLocation(shaderGrid, "eyePos");
        locTimeGrid = gl.glGetUniformLocation(shaderGrid, "time");
        locLightDirGrid = gl.glGetUniformLocation(shaderGrid, "lightDir");
        locLightCutoffGrid = gl.glGetUniformLocation(shaderGrid, "lightCutoff");
        locLightDistGrid = gl.glGetUniformLocation(shaderGrid, "lightDist");
        locShapeSetGrid = gl.glGetUniformLocation(shaderGrid, "shapeSet");
        locTexSetGrid = gl.glGetUniformLocation(shaderGrid, "texSet");
        locLightSetGrid = gl.glGetUniformLocation(shaderGrid, "lightSet");
        locAnimSetGrid = gl.glGetUniformLocation(shaderGrid, "animSet");
        locShaderSetGrid = gl.glGetUniformLocation(shaderGrid, "shaderSet");
        locIsTexGrid = gl.glGetUniformLocation(shaderGrid, "isTex");
        locParallaxSet = gl.glGetUniformLocation(shaderGrid, "parallaxSet");
        locIsOcclusion = gl.glGetUniformLocation(shaderGrid, "isOcclusion");
        locIsEnvGrid = gl.glGetUniformLocation(shaderGrid, "isEnv");
        locEnvMapSet= gl.glGetUniformLocation(shaderGrid, "envMapSet");
        locRefractIndex = gl.glGetUniformLocation(shaderGrid, "refractIndex");

        locDiff = gl.glGetUniformLocation(shaderGrid, "diff");
        locSpec = gl.glGetUniformLocation(shaderGrid, "spec");
        locAmbi = gl.glGetUniformLocation(shaderGrid, "ambi");
        locLightCol = gl.glGetUniformLocation(shaderGrid, "lightCol");
    }

    void createSkybox(GL2GL3 gl) {
        float[] cube = {
                // bottom (z-) face
                1, 0, 0,	0, 0, -1,
                0, 0, 0,	0, 0, -1,
                1, 1, 0,	0, 0, -1,
                0, 1, 0,	0, 0, -1,
                // top (z+) face
                1, 0, 1,	0, 0, 1,
                0, 0, 1,	0, 0, 1,
                1, 1, 1,	0, 0, 1,
                0, 1, 1,	0, 0, 1,
                // x+ face
                1, 1, 0,	1, 0, 0,
                1, 0, 0,	1, 0, 0,
                1, 1, 1,	1, 0, 0,
                1, 0, 1,	1, 0, 0,
                // x- face
                0, 1, 0,	-1, 0, 0,
                0, 0, 0,	-1, 0, 0,
                0, 1, 1,	-1, 0, 0,
                0, 0, 1,	-1, 0, 0,
                // y+ face
                1, 1, 0,	0, 1, 0,
                0, 1, 0,	0, 1, 0,
                1, 1, 1,	0, 1, 0,
                0, 1, 1,	0, 1, 0,
                // y- face
                1, 0, 0,	0, -1, 0,
                0, 0, 0,	0, -1, 0,
                1, 0, 1,	0, -1, 0,
                0, 0, 1,	0, -1, 0
        };

        int[] indexBufferData = new int[36];
        for (int i = 0; i<6; i++){
            indexBufferData[i*6] = i*4;
            indexBufferData[i*6 + 1] = i*4 + 1;
            indexBufferData[i*6 + 2] = i*4 + 2;
            indexBufferData[i*6 + 3] = i*4 + 1;
            indexBufferData[i*6 + 4] = i*4 + 2;
            indexBufferData[i*6 + 5] = i*4 + 3;
        }
        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 3),
                new OGLBuffers.Attrib("inNormal", 3)
        };

        skyBox = new OGLBuffers(gl, cube, attributes, indexBufferData);
    }


    void createTexture(GL2GL3 gl) {

        gl.glPixelStorei(GL2GL3.GL_UNPACK_ALIGNMENT,1);

        diffTex = new ArrayList<>();
        normTex = new ArrayList<>();
        heightTex = new ArrayList<>();
        for (Texture texture: Texture.values()) {
            String texTmp = "/textures/bricks";
            switch (texture) {
                case BRICKS:
                    texTmp = "/textures/bricks";
                    break;
                case ROCK:
                    texTmp = "/textures/rock";
                    break;
                case WALL:
                    texTmp = "/textures/wall2";
                    break;
            }
            diffTex.add(new OGLTexture2D(gl, texTmp + ".jpg"));
            normTex.add(new OGLTexture2D(gl, texTmp + "n.png"));
            heightTex.add(new OGLTexture2D(gl, texTmp + "h.png"));

        }

        cubeTex = new ArrayList<>();
        cubeTex.add(new OGLTextureCube(gl, "/textures/snow_.jpg", OGLTextureCube.SUFFICES_POSITIVE_NEGATIVE_FLIP_Y));
        cubeTex.add(new OGLTextureCube(gl, "/textures/church_.png", OGLTextureCube.SUFFICES_POSITIVE_NEGATIVE_FLIP_Y));
        cubeTex.add(new OGLTextureCube(gl, "/textures/forest_.png", OGLTextureCube.SUFFICES_POSITIVE_NEGATIVE_FLIP_Y));
        cubeTex.add(new OGLTextureCube(gl, "/textures/lake_.png", OGLTextureCube.SUFFICES_POSITIVE_NEGATIVE_FLIP_Y));

        for (int i = 0; i < Skybox.values().length; i++) {
            for (int j = 0; j < 6; j++) {
                skyTexs[i][j] = cubeTex.get(i).getTexImage(new OGLTexImageByte.Format(4), j);
            }
        }

        envTex = new ArrayList<>();

        for (int i = 0; i < Skybox.values().length; i++) {
            envTex.add(new OGLTexture2D(gl, skyTexs[i][0]));
        }
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {

    }

    private void gridScene(GL2GL3 gl, boolean forTexture) {
        Vec3D lightPos = new Vec3D(100, 20, 5);
        Vec3D diff = new Vec3D(0.3);
        Vec3D spec = new Vec3D(1);
        Vec3D ambi = new Vec3D(0.6);
        Vec3D lightCol = new Vec3D(1, 0.9, 0.9);
        switch (settings.getSkybox()) {
            case SNOW:
                lightPos = new Vec3D(100, 100, 100);
                diff = new Vec3D(0.8);
                spec = new Vec3D(1);
                ambi = new Vec3D(0.11, 0.13, 0.17);
                lightCol = new Vec3D(0.93, 0.86, 0.63);
                break;
            case CHURCH:
                lightPos = new Vec3D(0, 0, 100);
                diff = new Vec3D(0.8);
                spec = new Vec3D(1);
                ambi = new Vec3D(0.13, 0.11, 0.06);
                lightCol = new Vec3D(1, 0.96, 0.89);
                break;
            case FOREST:
                lightPos = new Vec3D(0, 0, 100);
                diff = new Vec3D(0.8);
                spec = new Vec3D(1);
                ambi = new Vec3D(0.53);
                lightCol = new Vec3D(0.93);
                break;
            case LAKE:
                lightPos = new Vec3D(100, 20, 5);
                diff = new Vec3D(0.8);
                spec = new Vec3D(0.9);
                ambi = new Vec3D(0.11, 0.15, 0.15);
                lightCol = new Vec3D(0.99, 0.95, 0.51);
                break;
        }

        gl.glUseProgram(shaderGrid);
        if(forTexture)
            gl.glUniformMatrix4fv(locMatGrid, 1, false,
                    ToFloatArray.convert(controller.getCam().getViewMatrix().mul(envProj)), 0);
        else
            gl.glUniformMatrix4fv(locMatGrid, 1, false,
                    ToFloatArray.convert(controller.getCam().getViewMatrix().mul(proj)), 0);
        gl.glUniformMatrix4fv(locTMatGrid, 1, false,
                ToFloatArray.convert(new Mat4Identity()), 0);
        gl.glUniformMatrix4fv(locProjGrid, 1, false,
                ToFloatArray.convert(proj), 0);
        gl.glUniform3fv(locEyeGrid, 1, ToFloatArray.convert(controller.getCam().getEye()), 0);
        gl.glUniform1f(locTimeGrid, time);
        switch (settings.getLight()) {
            case STATIC:
                gl.glUniform3fv(locLightGrid, 1, ToFloatArray.convert(lightPos), 0);
                gl.glUniform3fv(locLightDirGrid, 1, ToFloatArray.convert(
                        new Vec3D(-5, -5, -1.5)
                ), 0);
                break;
            case ANIMATION:
                gl.glUniform3fv(locLightGrid, 1, ToFloatArray.convert(
                        new Vec3D(5, 5, 0).mul(new Vec3D(Math.cos(time), Math.sin(time), 0))
                ), 0);
                gl.glUniform3fv(locLightDirGrid, 1, ToFloatArray.convert(
                        new Vec3D(-5, -5, 0).mul(new Vec3D(Math.cos(time), Math.sin(time), 0))
                ), 0);
                break;
            case CAMERA:
                gl.glUniform3fv(locLightGrid, 1, ToFloatArray.convert(
                        controller.getCam().getEye()
                ), 0);
                gl.glUniform3fv(locLightDirGrid, 1, ToFloatArray.convert(
                        controller.getCam().getEye().add(
                                controller.getCam().getViewVector().mul(settings.getLightDist())
                        )
                ), 0);
                break;
        }

        gl.glUniform1f(locLightDistGrid, settings.getLightDist());
        gl.glUniform1f(locLightCutoffGrid, settings.getLightCutoff());
        gl.glUniform1f(locRefractIndex, settings.getRefract().getIndex());

        gl.glUniform1i(locShapeSetGrid, settings.getShape().ordinal());
        gl.glUniform1i(locTexSetGrid, settings.getTexture().ordinal());
        gl.glUniform1i(locLightGrid, settings.getLight().ordinal());
        gl.glUniform1i(locAnimSetGrid, settings.getAnimation().ordinal());
        gl.glUniform1i(locShaderSetGrid, settings.getShader().ordinal());
        gl.glUniform1i(locParallaxSet, settings.getParallax().ordinal());
        gl.glUniform1i(locEnvMapSet, settings.getEnvMap().ordinal());

        gl.glUniform1i(locIsTexGrid, settings.isTexture()? 1 : 0);
        gl.glUniform1i(locIsOcclusion, settings.isOcclusion()? 1 : 0);

        gl.glUniform1i(locIsEnvGrid, 1);


        diffTex.get(settings.getTexture().ordinal()).bind(shaderGrid, "diffTex", 0);
        normTex.get(settings.getTexture().ordinal()).bind(shaderGrid, "normTex", 1);
        heightTex.get(settings.getTexture().ordinal()).bind(shaderGrid, "heightTex", 2);

        cubeTex.get(skyEnum).bind(shaderGrid, "cubeTex", 3);

        gl.glUniform3fv(locLightCol, 1, ToFloatArray.convert(lightCol), 0);
        if(settings.getEnvMap() == EnvMap.GOLD)
            gl.glUniform3fv(locDiff, 1, ToFloatArray.convert(new Vec3D(1, 0.84, 0)), 0);

        if(!forTexture) {
            grid.draw(GL2GL3.GL_TRIANGLES, shaderGrid);
        }

        gl.glUniform3fv(locDiff, 1, ToFloatArray.convert(diff), 0);
        gl.glUniform3fv(locSpec, 1, ToFloatArray.convert(spec), 0);
        gl.glUniform3fv(locAmbi, 1, ToFloatArray.convert(ambi), 0);

        for (Mat4 object: settings.getObjects()) {
            gl.glUniformMatrix4fv(locTMatGrid, 1, false,
                    ToFloatArray.convert(object), 0);
            gl.glUniform1i(locShapeSetGrid, Shape.SPHERE.ordinal());
            gl.glUniform1i(locAnimSetGrid, Animation.OFF.ordinal());
            gl.glUniform1i(locIsEnvGrid, 0);

            grid2.draw(GL2GL3.GL_TRIANGLES, shaderGrid);
        }
    }

    private void skyboxScene(GL2GL3 gl, boolean forTexture) {
        gl.glUseProgram(shaderSky);
        if(forTexture)
            gl.glUniformMatrix4fv(locMatSky, 1, false,
                    ToFloatArray.convert(controller.getCam().getViewMatrix().mul(envProj)), 0);
        else
            gl.glUniformMatrix4fv(locMatSky, 1, false,
                    ToFloatArray.convert(controller.getCam().getViewMatrix().mul(proj)), 0);
        
        cubeTex.get(skyEnum).bind(shaderSky, "cubeTex", 3);

        skyBox.draw(GL2GL3.GL_TRIANGLES, shaderSky);
    }

    private void enviromenMapping(GL2GL3 gl) {
        int imgWidth = skyTexs[skyEnum][0].getWidth();
        int imgHeight = skyTexs[skyEnum][0].getHeight();

        Camera tmpCam = controller.getCam();

        // +x
        renderTarget.bind();
        controller.setCam(new Camera().withPosition(new Vec3D(0)));
        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        gridScene(gl, true);
        skyboxScene(gl, true);

        textureColor = renderTarget.getColorTexture();
        envTex.get(skyEnum).fromBufferedImage(scale(textureColor.toBufferedImage(), imgWidth, imgHeight));
        envTexs[skyEnum][0] = envTex.get(skyEnum).getTexImage(new OGLTexImageByte.Format(4), 0);

        // -x
        renderTarget.bind();
        controller.setCam(controller.getCam()
                .addAzimuth(Math.PI));
        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        gridScene(gl, true);
        skyboxScene(gl, true);

        textureColor = renderTarget.getColorTexture();
        envTex.get(skyEnum).fromBufferedImage(scale(textureColor.toBufferedImage(), imgWidth, imgHeight));
        envTexs[skyEnum][1] = envTex.get(skyEnum).getTexImage(new OGLTexImageByte.Format(4), 0);

        // -y
        renderTarget.bind();
        controller.setCam(controller.getCam()
                .addZenith(-Math.PI / 2).addAzimuth(-Math.PI / 2));
        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        gridScene(gl, true);
        skyboxScene(gl, true);
        textureColor = renderTarget.getColorTexture();
        envTex.get(skyEnum).fromBufferedImage(scale(textureColor.toBufferedImage(), imgWidth, imgHeight));
        envTexs[skyEnum][2] = envTex.get(skyEnum).getTexImage(new OGLTexImageByte.Format(4), 0);

        // +y
        renderTarget.bind();
        controller.setCam(controller.getCam()
                .addZenith(Math.PI));
        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        gridScene(gl, true);
        skyboxScene(gl, true);
        textureColor = renderTarget.getColorTexture();
        envTex.get(skyEnum).fromBufferedImage(scale(textureColor.toBufferedImage(), imgWidth, imgHeight));
        envTexs[skyEnum][3] = envTex.get(skyEnum).getTexImage(new OGLTexImageByte.Format(4), 0);

        // +z
        renderTarget.bind();
        controller.setCam(controller.getCam()
                .addZenith(-Math.PI / 2));
        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        gridScene(gl, true);
        skyboxScene(gl, true);
        textureColor = renderTarget.getColorTexture();
        envTex.get(skyEnum).fromBufferedImage(scale(textureColor.toBufferedImage(), imgWidth, imgHeight));
        envTexs[skyEnum][4] = envTex.get(skyEnum).getTexImage(new OGLTexImageByte.Format(4), 0);

        // -z
        renderTarget.bind();
        controller.setCam(controller.getCam()
                .addAzimuth(Math.PI));
        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        gridScene(gl, true);
        skyboxScene(gl, true);
        textureColor = renderTarget.getColorTexture();
        envTex.get(skyEnum).fromBufferedImage(scale(textureColor.toBufferedImage(), imgWidth, imgHeight));
        envTexs[skyEnum][5] = envTex.get(skyEnum).getTexImage(new OGLTexImageByte.Format(4), 0);

        controller.setCam(tmpCam);
    }

    private BufferedImage scale(BufferedImage src, int w, int h)
    {
        BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        int x, y;
        int ww = src.getWidth();
        int hh = src.getHeight();
        for (x = 0; x < w; x++) {
            for (y = 0; y < h; y++) {
                int col = src.getRGB(x * ww / w, y * hh / h);
                img.setRGB(x, y, col);
            }
        }
        return img;
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        time += 0.01;
        skyEnum = settings.getSkybox().ordinal();

        GL2GL3 gl = drawable.getGL().getGL2GL3();

        gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, polygonMode);

        if (settings.isRedraw()) {
            settings.setRedraw(false);
            enviromenMapping(gl);
        }

        gl.glBindFramebuffer(GL2GL3.GL_FRAMEBUFFER, 0);
        gl.glViewport(0, 0, width, height);

        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        skyboxScene(gl, false);
        for (int i = 0; i < envTexs[skyEnum].length; i++)
            cubeTex.get(skyEnum).setTexImage(envTexs[skyEnum][i], i);

        gridScene(gl, false);

        for (int i = 0; i < envTexs[skyEnum].length; i++)
            cubeTex.get(skyEnum).setTexImage(skyTexs[skyEnum][i], i);
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y,
                        int width, int height) {
        this.width = width;
        this.height = height;
        proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
        envProj = new Mat4PerspRH(Math.PI / 2, skyWidth / skyHeigh, 0.01, 1000.0);
    }
}